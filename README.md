# Projekt E

## Building

### Windows

Requires [Visual Studio Build Tools](https://visualstudio.microsoft.com/downloads/?q=build+tools#build-tools-for-visual-studio-2019) or Visual Studio 2019 or newer. Must be ran inside "x64 Native Tool Command Prompt for VS". Tested on Windows 10.

Currently only supports release builds.

```
build.bat
```

### Linux

Requires GCC 11 or newer. Tested on Ubuntu 20.04 LTS.

Currently only supports release builds.

```
sh ./build.sh
```

## Running and testing

The executable and all required dynamic libraries are in `run_tree/<os>/`.

```
run_tree\win\app        # run on windows
run_tree\linux\app      # run on linux
```

To run tests, run the executable with the `test` argument.

```
run_tree\<platform>\app test
```

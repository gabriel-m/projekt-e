g++ \
src/app.cpp src/common.cpp \
-g -o run_tree/linux/app \
-Wno-format \
-fno-strict-aliasing -O2 \
-DMKL_ILP64 -m64 \
-Wl,-rpath='${ORIGIN}' \
-Wl,--start-group \
lib/linux_x64/libmkl_intel_ilp64.a lib/linux_x64/libmkl_core.a lib/linux_x64/libmkl_intel_thread.a \
-Wl,--end-group \
-Wl,--start-group run_tree/linux/libiomp5.so -Wl,--end-group \
-static-libgcc -ldl -lm -lpthread

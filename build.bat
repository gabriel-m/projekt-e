@echo off
pushd %~dp0

del run_tree\win\app.exe 2>NUL
del run_tree\win\app.pdb 2>NUL

cl -nologo ^
   -Zc:referenceBinding -Zc:strictStrings -Zc:rvalueCast ^
   -Z7 -EHa- -GR- -MP -Gm- -fp:fast -fp:except- -we4239 ^
   -O2 -MT ^
   -D_CRT_SECURE_NO_WARNINGS ^
   -DMKL_ILP64 ^
   -Ferun_tree\win\app.exe ^
   src\app.cpp src\common.cpp ^
   kernel32.lib ^
   libcmt.lib libcpmt.lib libvcruntime.lib libucrt.lib ^
   lib\win_x64\mkl_intel_ilp64.lib lib\win_x64\mkl_core.lib lib\win_x64\mkl_intel_thread.lib lib\win_x64\libiomp5md.lib ^
   -link -nodefaultlib -debug -incremental:no -opt:ref -opt:icf -subsystem:console

del app.obj    2>NUL
del common.obj 2>NUL

popd

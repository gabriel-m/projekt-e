#include <time.h>


EnterApplicationNamespace


static QPC qpc_resolution_nanoseconds;
GlobalBlock
{
    timespec resolution;
    int status = clock_getres(CLOCK_MONOTONIC_RAW, &resolution);
    assert(status >= 0);
    assert(resolution.tv_sec == 0);
    qpc_resolution_nanoseconds = (QPC)resolution.tv_nsec;
};

QPC current_qpc()
{
    timespec timestamp;
    int status = clock_gettime(CLOCK_MONOTONIC_RAW, &timestamp);
    assert(status >= 0);
    return (QPC)timestamp.tv_sec * 1'000'000'000ll + timestamp.tv_nsec;
}

double seconds_from_qpc(QPC qpc)
{
    return (double)(qpc * qpc_resolution_nanoseconds) / 1e9;
}

QPC qpc_from_seconds(double seconds)
{
    return (QPC)(seconds * 1'000'000'000 / qpc_resolution_nanoseconds);
}


ExitApplicationNamespace

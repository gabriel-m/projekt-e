#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <windows.h>


EnterApplicationNamespace


////////////////////////////////////////////////////////////////////////////////////////////////////////
// Time
////////////////////////////////////////////////////////////////////////////////////////////////////////

static QPC qpc_frequency;
GlobalBlock
{
    LARGE_INTEGER frequency;
    BOOL ok = QueryPerformanceFrequency(&frequency);
    assert(ok);
    qpc_frequency = frequency.QuadPart;
};

QPC current_qpc()
{
    LARGE_INTEGER counter;
    BOOL ok = QueryPerformanceCounter(&counter);
    assert(ok);
    return counter.QuadPart;
}

double seconds_from_qpc(QPC qpc)
{
    return (double) qpc / (double) qpc_frequency;
}

QPC qpc_from_seconds(double seconds)
{
    return (QPC)(seconds * (double) qpc_frequency);
}


ExitApplicationNamespace

#include <stdio.h>
#include <omp.h>

#include "common.h"
#include "libraries/onemkl/mkl.h"

EnterApplicationNamespace

static constexpr double EPSILON = 1e-8;

static inline bool compare_f64(f64 a, f64 b)
{
    return (b - a < EPSILON) || (a - b < EPSILON);
}

static inline bool compare_z64(MKL_Complex16 a, MKL_Complex16 b)
{
    return ((b.real - a.real < EPSILON) || (a.real - b.real < EPSILON) &&
            (b.imag - a.imag < EPSILON) || (a.imag - b.imag < EPSILON));
}

static inline double sin_f64(double x)
{
    double result;
    vdSin(1, &x, &result);
    return result;
}

static inline double cos_f64(double x)
{
    double result;
    vdCos(1, &x, &result);
    return result;
}

static inline double acos_f64(double x)
{
    double result;
    vdAcos(1, &x, &result);
    return result;
}

static inline MKL_Complex16 acos_z64(MKL_Complex16 x)
{
    MKL_Complex16 result;
    vzAcos(1, &x, &result);
    return result;
}

static inline MKL_Complex16 exp_complex(MKL_Complex16 x)
{
    MKL_Complex16 result;
    vzExp(1, &x, &result);
    return result;
}

static inline MKL_Complex16 mul_complex(MKL_Complex16 a, MKL_Complex16 b)
{
    MKL_Complex16 result;
    vzMul(1, &a, &b, &result);
    return result;
}

enum Matrix_Operation_Flags : u32
{
    CONJUGATE_TRANSPOSE_FIRST  = (1U << 0),
    CONJUGATE_TRANSPOSE_SECOND = (1U << 1),
};

struct Matrix_Real
{
    umm     rows;
    umm     cols;
    double* data;
};

template<bool zero_memory = true>
static inline void allocate_matrix(Matrix_Real* matrix)
{
    matrix->data = allocate_array_on_application_heap<double, zero_memory>(matrix->rows * matrix->cols);
}

static void free_matrix(Matrix_Real* matrix)
{
    free(matrix->data);
    *matrix = {};
}

static inline double* cell(Matrix_Real* matrix, umm row, umm col)
{
    DebugAssert(row < matrix->rows && col < matrix->cols);
    return matrix->data + (row * matrix->cols) + col;
}

static inline void allocate_for_multiplication(Matrix_Real* out, Matrix_Real* a, Matrix_Real* b)
{
    DebugAssert(b->rows == a->cols);
    out->rows = a->rows;
    out->cols = b->cols;
    allocate_matrix(out);
}

static inline void mul(Matrix_Real* out, Matrix_Real* a, Matrix_Real* b)
{
    DebugAssert(  b->rows == a->cols);
    DebugAssert(out->rows == a->cols);
    DebugAssert(out->cols == b->cols);

    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, a->rows, b->cols, a->cols, 1 /* alpha */,
                a->data, a->cols, b->data, b->cols, 0 /* beta */, out->data, out->cols);
}

struct Matrix_Complex
{
    umm            rows;
    umm            cols;
    MKL_Complex16* data;
};

template<bool zero_memory = true>
static inline void allocate_matrix(Matrix_Complex* matrix)
{
    matrix->data = allocate_array_on_application_heap<MKL_Complex16, zero_memory>(matrix->rows * matrix->cols);
}

static void free_matrix(Matrix_Complex* matrix)
{
    free(matrix->data);
    *matrix = {};
}

static inline MKL_Complex16* cell(Matrix_Complex* matrix, umm row, umm col)
{
    DebugAssert(row < matrix->rows && col < matrix->cols);
    return matrix->data + (row * matrix->cols) + col;
}

static void print(Matrix_Complex* m)
{
    for (umm y = 0; y < m->rows; y++)
    {
        for (umm x = 0; x < m->rows; x++)
        {
            MKL_Complex16* n = cell(m, x, y);
            if (n->real >= 0.0)
                printf(" ");

            char imag_sign = '+';
            double imag_for_write = n->imag;
            if (n->imag < 0.0 || (n->imag == 0.0 && (1 / n->imag) < 0))
            {
                imag_sign = '-';
                imag_for_write = -n->imag;
            }

            printf("%.2f%ci%.2f ", n->real, imag_sign, imag_for_write);
        }
        printf("\n");
    }
    printf("\n");
}

static inline void allocate_for_multiplication(Matrix_Complex* out, Matrix_Complex* a, Matrix_Complex* b)
{
    DebugAssert(b->rows == a->cols);
    out->rows = a->rows;
    out->cols = b->cols;
    allocate_matrix(out);
}

static inline void zero(Matrix_Complex* m)
{
    memset(m->data, 0x0, m->rows * m->cols * sizeof(MKL_Complex16));
}

static inline void scale(Matrix_Complex* m, MKL_Complex16 scalar_factor)
{
    DebugAssert(m->rows && m->cols);
    mkl_zimatcopy('R', 'N', m->rows, m->cols, scalar_factor, m->data, m->cols, m->cols);
}

static inline void conjugate_transpose(Matrix_Complex* m, MKL_Complex16 scalar_factor = { 1.0, 0.0 })
{
    DebugAssert(m->rows && m->cols);
    mkl_zimatcopy('R', 'C', m->rows, m->cols, scalar_factor, m->data, m->cols, m->rows);
}

static inline void inv(Matrix_Complex* m)
{
    DebugAssert(m->rows == m->cols);

    umm min = m->rows;
    if (min > m->cols)
        min = m->cols;

    lapack_int* ipiv = allocate_array_on_application_heap<lapack_int>(min);
    LAPACKE_zgetrf(LAPACK_ROW_MAJOR, m->rows, m->cols, m->data, m->cols, ipiv);
    LAPACKE_zgetri(LAPACK_ROW_MAJOR, m->rows, m->data, m->cols, ipiv);
    free(ipiv);
}

static inline void mul(Matrix_Complex* out, Matrix_Complex* a, Matrix_Complex* b, MKL_Complex16 scalar_factor = { 1.0, 0.0 }, Matrix_Operation_Flags flags = {})
{
    DebugAssert(  b->rows == a->cols);
    DebugAssert(out->rows == a->cols);
    DebugAssert(out->cols == b->cols);

    //static constexpr MKL_Complex16 alpha = { 1, 0 };
    static constexpr MKL_Complex16 beta  = { 0, 0 };

    cblas_zgemm(CblasRowMajor,
        (flags & CONJUGATE_TRANSPOSE_FIRST)  ? CblasConjTrans : CblasNoTrans,
        (flags & CONJUGATE_TRANSPOSE_SECOND) ? CblasConjTrans : CblasNoTrans,
        a->rows, b->cols, a->cols, &scalar_factor,
        a->data, a->cols, b->data, b->cols, &beta, out->data, out->cols);
}

static inline void sub(Matrix_Complex* out, Matrix_Complex* a, Matrix_Complex* b, MKL_Complex16 scalar_factor = { 1.0, 0.0 })
{
    DebugAssert(out->rows == a->rows && a->rows == b->rows);
    DebugAssert(out->cols == a->cols && a->cols == b->cols);
    vzSub(out->rows * out->cols, a->data, b->data, out->data);
}

// matrix must be square.
// diagonal == 0 => main diagonal
// diagonal  > 0 => n-th diagonal above the main one
// diagonal  < 0 => n-th diagonal below the main one
static inline void set_diagonal(Matrix_Complex* m, smm diagonal, MKL_Complex16 c)
{
    assert(m->rows == m->cols);
    assert((diagonal < 0 ? -diagonal : diagonal) < m->rows);

    if (diagonal >= 0)
    {
        for (smm y = 0; y < (smm)m->cols - diagonal; y++)
            *cell(m, y + diagonal, y) = c;
    }
    else
    {
        for (smm y = -diagonal; y < (smm)m->cols; y++)
            *cell(m, y + diagonal, y) = c;
    }
}


static void init_onemkl()
{
    int logical_core_count = omp_get_num_procs();
    printf("%d logical cores, assuming 1 cpu system.\n", logical_core_count);
    mkl_set_num_threads(logical_core_count);
}

struct Quantum_Transport_Datapoint
{
    double energy;
    double transmission;
};

static void quantum_transport_1d_atom_chain(
    Quantum_Transport_Datapoint* result, umm result_count,
    umm atom_count, double t,
    double energy_from, double energy_to)
{
    constexpr MKL_Complex16 zplus = { 0.0, 1e-12 };

    Matrix_Complex eye          = { atom_count, atom_count };
    Matrix_Complex h            = { atom_count, atom_count };
    Matrix_Complex sigma1       = { atom_count, atom_count };
    Matrix_Complex sigma2       = { atom_count, atom_count };
    Matrix_Complex sigma1_trans = { atom_count, atom_count };
    Matrix_Complex sigma2_trans = { atom_count, atom_count };
    Matrix_Complex gamma1       = { atom_count, atom_count };
    Matrix_Complex gamma2       = { atom_count, atom_count };
    Matrix_Complex g            = { atom_count, atom_count };
    allocate_matrix(&eye);
    allocate_matrix(&h);
    allocate_matrix(&sigma1);
    allocate_matrix(&sigma2);
    allocate_matrix(&sigma1_trans);
    allocate_matrix(&sigma2_trans);
    allocate_matrix(&gamma1);
    allocate_matrix(&gamma2);
    allocate_matrix(&g);
    Defer(free_matrix(&eye));
    Defer(free_matrix(&h));
    Defer(free_matrix(&sigma1));
    Defer(free_matrix(&sigma2));
    Defer(free_matrix(&sigma1_trans));
    Defer(free_matrix(&sigma2_trans));
    Defer(free_matrix(&gamma1));
    Defer(free_matrix(&gamma2));
    Defer(free_matrix(&g));

    set_diagonal(&h, -1, { -t, 0 });
    set_diagonal(&h, +1, { -t, 0 });

    #pragma omp parallel for num_threads(16)
    for (umm i = 0; i < result_count; i++)
    {
        double energy = energy_from * t + (i / (double)result_count) * (energy_to - energy_from) * t;

        zero(&sigma1_trans);
        zero(&sigma2_trans);
        zero(&gamma1);
        zero(&g);

        MKL_Complex16 ka;
        if (energy >= -1.0 && energy <= 1.0)
        {
            ka = { 0, acos_f64(-energy / (2 * t)) };
        }
        else
        {
            ka = acos_z64({ -energy / (2 * t), 0 });
            ka = mul_complex(ka, { 0, 1 });
        }

        MKL_Complex16 sigma = mul_complex({ -t, 0 }, exp_complex(ka));

        *cell(&sigma1,       0, 0) = sigma;
        *cell(&sigma1_trans, 0, 0) = sigma;
        *cell(&sigma2,       sigma2.rows       - 1, sigma2.cols       - 1) = sigma;
        *cell(&sigma2_trans, sigma2_trans.rows - 1, sigma2_trans.cols - 1) = sigma;

        conjugate_transpose(&sigma1_trans);
        conjugate_transpose(&sigma2_trans);

        sub  (&gamma1, &sigma1, &sigma1_trans);
        scale(&gamma1, { 0.0, 1.0 });

        sub  (&gamma2, &sigma2, &sigma2_trans);
        scale(&gamma2, { 0.0, 1.0 });

        MKL_Complex16 x = zplus;
        x.real += energy;
        set_diagonal(&eye, 0, x);

        sub(&g, &eye,  &h);
        sub(&g, &g, &sigma1);
        sub(&g, &g, &sigma2);
        inv(&g);

        Matrix_Complex temp[] = {
            sigma1_trans,
            sigma2_trans,
        };

        mul(&temp[0], &gamma1,  &g);
        mul(&temp[1], &temp[0], &gamma2);
        mul(&temp[0], &temp[1], &g, { 1, 0 }, CONJUGATE_TRANSPOSE_SECOND);

        double transmission = 0.0;
        for (umm i = 0; i < temp[0].rows; i++)
            transmission += cell(&temp[0], i, i)->real;

        result[i] = { energy, transmission };
    }

#if 0
    Es = linspace(-2*t, 2*t, 500);
    Ts = zeros(1, length(Es));

    sigma1 = zeros(N, N);
    sigma2 = zeros(N, N);
    I      = eye(N, N);
    for i = 1:length(Es)
        ene = Es(i);

        ka    = acos(-ene / (2*t));
        sigma = -t * exp(1i * ka);

        sigma1(1,   1)   = sigma; % top-left non zero
        sigma2(end, end) = sigma; % bottom-right non zero

        gamma1 = 1i * (sigma1 - sigma1');
        gamma2 = 1i * (sigma2 - sigma2');

        G = inv((ene + zplus) * I - H - sigma1 - sigma2);
        Ts(i) = real(trace(gamma1 * G * gamma2 * G'));
    end
#endif
}




static void test()
{
    init_onemkl();

    printf("Running tests...\n");

    umm test_count = 0;
    QPC test_start = 0;
    QPC test_end   = 0;
    auto declare_test = [&](const char* name)
    {
        umm test = ++test_count;
        printf("Test %llu: %-80s", test, name);
        test_start = current_qpc();
    };
    auto complete_test_and_report_time = [&]()
    {
        QPC duration = test_end - test_start;
        test_start = 0;
        test_start = 0;
        printf("OK. Took %8.3f ms\n", seconds_from_qpc(duration) * 1000.0);
    };

    declare_test("1D atom line (501 atoms)");
    {
        Quantum_Transport_Datapoint* array = allocate_array_on_application_heap<Quantum_Transport_Datapoint>(500);
        quantum_transport_1d_atom_chain(array, 500, 501, 1.0, -3, 3);
        free(array);
        test_end = current_qpc();
    }
    complete_test_and_report_time();

    declare_test("(real) identity == identity * identity");
    {
        Matrix_Real ident = { 10, 10 };
        allocate_matrix(&ident);
        Defer(free_matrix(&ident));

        for (umm i = 0; i < ident.rows; i++)
            *cell(&ident, i, i) = 1.0;

        Matrix_Real result = {};
        allocate_for_multiplication(&result, &ident, &ident);
        Defer(free_matrix(&result));

        test_start = current_qpc();
        mul(&result, &ident, &ident);
        test_end = current_qpc();

        for (umm row = 0; row < result.rows; row++)
            for (umm col = 0; col < result.cols; col++)
            {
                if (row == col) Assert(compare_f64(*cell(&result, row, col), 1.0));
                else            Assert(compare_f64(*cell(&result, row, col), 0.0));
            }
    }
    complete_test_and_report_time();

    declare_test("(real) x == identity * x");
    {
        Matrix_Real ident = { 10, 10 };
        allocate_matrix(&ident);
        Defer(free_matrix(&ident));

        for (umm i = 0; i < ident.rows; i++)
            *cell(&ident, i, i) = 1.0;

        Matrix_Real b = { 10, 10 };
        allocate_matrix(&b);
        Defer(free_matrix(&b));
        for (umm row = 0; row < b.rows; row++)
            for (umm col = 0; col < b.cols; col++)
                *cell(&b, row, col) = (col - row) * (col - row);

        Matrix_Real result = {};
        allocate_for_multiplication(&result, &ident, &b);
        Defer(free_matrix(&result));

        test_start = current_qpc();
        mul(&result, &ident, &b);
        test_end = current_qpc();

        for (umm row = 0; row < result.rows; row++)
            for (umm col = 0; col < result.cols; col++)
                Assert(compare_f64(*cell(&result, row, col), *cell(&b, row, col)));
    }
    complete_test_and_report_time();

    declare_test("(real) [1 2; 3 4] * [1; 2] == [5; 11]");
    {
        Matrix_Real a = { 2, 2 };
        allocate_matrix(&a);
        Defer(free_matrix(&a));
        *cell(&a, 0, 0) = 1;
        *cell(&a, 0, 1) = 2;
        *cell(&a, 1, 0) = 3;
        *cell(&a, 1, 1) = 4;

        Matrix_Real b = { 2, 1 };
        allocate_matrix(&b);
        Defer(free_matrix(&b));
        *cell(&b, 0, 0) = 1;
        *cell(&b, 0, 1) = 2;

        Matrix_Real result = {};
        allocate_for_multiplication(&result, &a, &b);
        Defer(free_matrix(&result));

        test_start = current_qpc();
        mul(&result, &a, &b);
        test_end = current_qpc();

        Assert(result.rows == 2 && result.cols == 1);

        Assert(compare_f64(*cell(&result, 0, 0), 5));
        Assert(compare_f64(*cell(&result, 0, 1), 11));
    }
    complete_test_and_report_time();

    declare_test("(real) 1000x1000 identity == identity * identity");
    {
        Matrix_Real ident = { 1000, 1000 };
        allocate_matrix(&ident);
        Defer(free_matrix(&ident));

        for (umm i = 0; i < ident.rows; i++)
            *cell(&ident, i, i) = 1.0;

        Matrix_Real result = {};
        allocate_for_multiplication(&result, &ident, &ident);
        Defer(free_matrix(&result));

        test_start = current_qpc();
        mul(&result, &ident, &ident);
        test_end = current_qpc();

        for (umm row = 0; row < result.rows; row++)
            for (umm col = 0; col < result.cols; col++)
            {
                if (row == col) Assert(compare_f64(*cell(&result, row, col), 1.0));
                else            Assert(compare_f64(*cell(&result, row, col), 0.0));
            }
    }
    complete_test_and_report_time();

    declare_test("(complex) identity == identity * identity");
    {
        Matrix_Complex ident = { 10, 10 };
        allocate_matrix(&ident);
        Defer(free_matrix(&ident));

        for (umm i = 0; i < ident.rows; i++)
            *cell(&ident, i, i) = { 1, 0 };

        Matrix_Complex result = {};
        allocate_for_multiplication(&result, &ident, &ident);
        Defer(free_matrix(&result));

        test_start = current_qpc();
        mul(&result, &ident, &ident);
        test_end = current_qpc();

        for (umm row = 0; row < result.rows; row++)
            for (umm col = 0; col < result.cols; col++)
            {
                if (row == col) Assert(compare_z64(*cell(&result, row, col), { 1, 0 }));
                else            Assert(compare_z64(*cell(&result, row, col), { 0, 0 }));
            }
    }
    complete_test_and_report_time();

    declare_test("(complex) x == identity * x");
    {
        Matrix_Complex ident = { 10, 10 };
        allocate_matrix(&ident);
        Defer(free_matrix(&ident));

        for (umm i = 0; i < ident.rows; i++)
            *cell(&ident, i, i) = { 1, 0 };

        Matrix_Complex b = { 10, 10 };
        allocate_matrix(&b);
        Defer(free_matrix(&b));
        for (umm row = 0; row < b.rows; row++)
            for (umm col = 0; col < b.cols; col++)
                *cell(&b, row, col) = { double((col - row) * (col - row)), (double)(col - 2*row) };

        Matrix_Complex result = {};
        allocate_for_multiplication(&result, &ident, &b);
        Defer(free_matrix(&result));

        test_start = current_qpc();
        mul(&result, &ident, &b);
        test_end = current_qpc();

        for (umm row = 0; row < result.rows; row++)
            for (umm col = 0; col < result.cols; col++)
                Assert(compare_z64(*cell(&result, row, col), *cell(&b, row, col)));
    }
    complete_test_and_report_time();

    declare_test("(complex) [1+i 2-i; 3+2i 4-2i] * [1+3i; 2-3i] == [-1-4i; -1-5i]");
    {
        Matrix_Complex a = { 2, 2 };
        allocate_matrix(&a);
        Defer(free_matrix(&a));
        *cell(&a, 0, 0) = { 1,  1 };
        *cell(&a, 0, 1) = { 2, -1 };
        *cell(&a, 1, 0) = { 3,  2 };
        *cell(&a, 1, 1) = { 4, -2 };

        Matrix_Complex b = { 2, 1 };
        allocate_matrix(&b);
        Defer(free_matrix(&b));
        *cell(&b, 0, 0) = { 1,  3 };
        *cell(&b, 0, 1) = { 2, -3 };

        Matrix_Complex result = {};
        allocate_for_multiplication(&result, &a, &b);
        Defer(free_matrix(&result));

        test_start = current_qpc();
        mul(&result, &a, &b);
        test_end = current_qpc();

        Assert(result.rows == 2 && result.cols == 1);

        Assert(compare_z64(*cell(&result, 0, 0), { -1, -4 }));
        Assert(compare_z64(*cell(&result, 0, 1), { -1, -5 }));
    }
    complete_test_and_report_time();

    declare_test("(complex) 1000x1000 identity == identity * identity");
    {
        Matrix_Complex ident = { 1000, 1000 };
        allocate_matrix(&ident);
        Defer(free_matrix(&ident));

        for (umm i = 0; i < ident.rows; i++)
            *cell(&ident, i, i) = { 1, 0 };

        Matrix_Complex result = {};
        allocate_for_multiplication(&result, &ident, &ident);
        Defer(free_matrix(&result));

        test_start = current_qpc();
        mul(&result, &ident, &ident);
        test_end = current_qpc();

        for (umm row = 0; row < result.rows; row++)
            for (umm col = 0; col < result.cols; col++)
            {
                if (row == col) Assert(compare_z64(*cell(&result, row, col), { 1, 0 }));
                else            Assert(compare_z64(*cell(&result, row, col), { 0, 0 }));
            }
    }
    complete_test_and_report_time();

}


static void entry()
{
    init_onemkl();

    constexpr umm COUNT = 500;
    Quantum_Transport_Datapoint* array = allocate_array_on_application_heap<Quantum_Transport_Datapoint>(COUNT);
    Defer(free(array));
    quantum_transport_1d_atom_chain(array, COUNT, 5, 1.0, -3, 3);

    FILE* f = fopen("out.csv", "w");
    for (umm i = 0; i < COUNT; i++)
        fprintf(f, "%f,%f\n", array[i].energy, array[i].transmission);
    fclose(f);
}


ExitApplicationNamespace


int main(int argc, char* argv[])
{
    if (argc == 2 && strcmp(argv[1], "test") == 0)
    {
        ApplicationNamespace::test();
        return 0;
    }

    ApplicationNamespace::entry();
    return 0;
}



#if defined(OS_WINDOWS)
#include "windows/mkl.h"
#elif defined(OS_LINUX)
#include "linux/mkl.h"
#else
#error "Unsupported OS"
#endif
#include <stdlib.h>
#include <stdio.h>
#include "common.h"

#if defined(OS_WINDOWS)
#include "common_windows.inl"
#elif defined(OS_LINUX)
#include "common_linux.inl"
#else
#error "Unsupported OS"
#endif

EnterApplicationNamespace


ExitApplicationNamespace

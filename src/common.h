#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>


#define ApplicationNamespace            app
#define EnterApplicationNamespace       namespace ApplicationNamespace {
#define ExitApplicationNamespace        }


EnterApplicationNamespace


////////////////////////////////////////////////////////////////////////////////
// Architecture, operating system, and compiler detection
////////////////////////////////////////////////////////////////////////////////


#if defined(__amd64__) || defined(__amd64) || defined(__x86_64__) || defined(__x86_64) || defined(_M_X64) || defined(_M_AMD64)
  #define ARCHITECTURE_X64
#else
  #error "Unsupported CPU architecture."
#endif

#if defined(_WIN32)
  #define OS_WINDOWS
#elif defined(__linux__)
  #define OS_LINUX
#else
  #error "Unrecognized operating system."
#endif

#if defined(_MSC_VER)
  #define COMPILER_MSVC
#elif defined(__GNUC__)
  #define COMPILER_GCC
#else
  #error "Unrecognized compiler."
#endif

#if defined(OS_WINDOWS)
 #include <intrin.h>
#elif defined(OS_LINUX)
 #include <xmmintrin.h>
#endif



////////////////////////////////////////////////////////////////////////////////
// Helper macros
////////////////////////////////////////////////////////////////////////////////


#define Concatenate__(x, y) x##y
#define Concatenate_(x, y)  Concatenate__(x, y)
#define Concatenate(x, y)   Concatenate_(x, y)

#define StringLiteral__(x)  #x
#define StringLiteral_(x)   StringLiteral__(x)
#define StringLiteral(x)    StringLiteral_(x)

#define UniqueIdentifier(name) Concatenate(_##name##_, __COUNTER__)


// Defer

template <typename F>
struct Defer_RAII
{
    F f;
    Defer_RAII(F f): f(f) {}
    ~Defer_RAII() { f(); }
};

template <typename F>
Defer_RAII<F> defer_function(F f)
{
    return Defer_RAII<F>(f);
}

#define Defer(code)   auto UniqueIdentifier(defer) = defer_function([&] () { code; })


// Global initializer

#define GlobalBlock GlobalBlock_(UniqueIdentifier(global_block))
#define GlobalBlock_(name) GlobalBlock__(name)
#define GlobalBlock__(name) \
    static struct name##_struct \
    { \
        typedef void Lambda(); \
        inline name##_struct(Lambda* f) { f(); } \
    } name##_instance = (name##_struct::Lambda*) []()

#define GlobalCleanupBlock GlobalCleanupBlock_(UniqueIdentifier(global_cleanup_block))
#define GlobalCleanupBlock_(name) GlobalCleanupBlock__(name)
#define GlobalCleanupBlock__(name) \
    static struct name##_struct \
    { \
        typedef void Lambda(); \
        void(*f)(); \
        inline name##_struct(void(*f)()): f(f) {} \
        inline ~name##_struct() { f(); } \
    } name##_instance = (name##_struct::Lambda*) []()

// Thread-local initializer

#define ThreadLocalBlock ThreadLocalBlock_(UniqueIdentifier(thread_local_block))
#define ThreadLocalBlock_(name) ThreadLocalBlock__(name)
#define ThreadLocalBlock__(name) \
    static thread_local struct name##_struct \
    { \
        typedef void Lambda(); \
        inline name##_struct(void(*f)()) { f(); } \
    } name##_instance = (name##_struct::Lambda*) []()

#define ThreadLocalCleanupBlock ThreadLocalCleanupBlock_(UniqueIdentifier(thread_local_cleanup_block))
#define ThreadLocalCleanupBlock_(name) ThreadLocalCleanupBlock__(name)
#define ThreadLocalCleanupBlock__(name) \
    static thread_local struct name##_struct \
    { \
        typedef void Lambda(); \
        Lambda* f; \
        inline name##_struct(Lambda* f): f(f) {} \
        inline ~name##_struct() { f(); } \
    } name##_instance = (name##_struct::Lambda*) []()

// Symbol import/export

#if defined(COMPILER_MSVC)
  #define ImportedSymbol extern "C" _declspec(dllimport)
  #define ExportedSymbol extern "C" _declspec(dllexport)
#elif defined(COMPILER_GCC)
  #if __GNUC__ >= 4
    #define ImportedSymbol extern "C" __attribute__((visibility("default")))
    #define ExportedSymbol extern "C" __attribute__((visibility("hidden")))
  #else
    #define ImportedSymbol extern "C"
    #define ExportedSymbol extern "C"
  #endif
#else
  #error "Unsupported compiler!"
#endif

#if defined(COMPILER_MSVC)
#define DoesNotReturn __declspec(noreturn)
#elif defined(COMPILER_GCC)
#define DoesNotReturn __attribute__((noreturn))
#else
#error "Unsupported compiler!"
#endif


////////////////////////////////////////////////////////////////////////////////
// Assert
////////////////////////////////////////////////////////////////////////////////


// @Incomplete make a good assert
//DoesNotReturn void assertion_failure(const char* message, const char* file, int line);
//#define Assert(test)                 { if ProbablyFalse(!(test)) assertion_failure(          "Assert(" #test ")", __FILE__, __LINE__); }
//#define AssertMessage(test, message) { if ProbablyFalse(!(test)) assertion_failure(message "\nAssert(" #test ")", __FILE__, __LINE__); }
#define Assert(test)                 do { assert(test); } while (0)
#define AssertMessage(test, message) do { assert(message, (test)); } while (0)


#if defined(DEBUG_BUILD)
#define DebugAssert(test) Assert(test)
#define DebugAssertMessage(test, message) AssertMessage(test, message)
#else
#define DebugAssert(test) {}
#define DebugAssertMessage(test, message) {}
#endif

#define Unreachable        assert(("Unreachable", false));
#define NotImplemented     assert(("Not implemented", false));
#define IllegalDefaultCase default: assert(("Illegal default case", false));

#define CompileTimeAssert(test) static_assert(test, "Compile-time assertion (" #test ") failed!")




////////////////////////////////////////////////////////////////////////////////
// Primitive types
////////////////////////////////////////////////////////////////////////////////


typedef int8_t  s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;
CompileTimeAssert(sizeof(s8)  == 1);
CompileTimeAssert(sizeof(s16) == 2);
CompileTimeAssert(sizeof(s32) == 4);
CompileTimeAssert(sizeof(s64) == 8);

constexpr s8  S8_MAX  = 0x7fl;
constexpr s16 S16_MAX = 0x7fffl;
constexpr s32 S32_MAX = 0x7fffffffl;
constexpr s64 S64_MAX = 0x7fffffffffffffffll;

constexpr s8  S8_MIN  = (s8)  0x80ul;
constexpr s16 S16_MIN = (s16) 0x8000ul;
constexpr s32 S32_MIN = (s32) 0x80000000ul;
constexpr s64 S64_MIN = (s64) 0x8000000000000000ull;

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
CompileTimeAssert(sizeof(u8)  == 1);
CompileTimeAssert(sizeof(u16) == 2);
CompileTimeAssert(sizeof(u32) == 4);
CompileTimeAssert(sizeof(u64) == 8);

constexpr u8  U8_MAX  = 0xfful;
constexpr u16 U16_MAX = 0xfffful;
constexpr u32 U32_MAX = 0xfffffffful;
constexpr u64 U64_MAX = 0xffffffffffffffffull;

#if defined(ARCHITECTURE_X86)
typedef s32 smm;
typedef u32 umm;
constexpr smm SMM_MIN = S32_MIN;
constexpr smm SMM_MAX = S32_MAX;
constexpr umm UMM_MAX = U32_MAX;
#else
typedef s64 smm;
typedef u64 umm;
constexpr smm SMM_MIN = S64_MIN;
constexpr smm SMM_MAX = S64_MAX;
constexpr umm UMM_MAX = U64_MAX;
#endif

CompileTimeAssert(sizeof(smm) == sizeof(void*));
CompileTimeAssert(sizeof(umm) == sizeof(void*));

typedef u8 byte;

typedef u8  flags8;
typedef u16 flags16;
typedef u32 flags32;
typedef u64 flags64;

typedef u8  mask8;
typedef u16 mask16;
typedef u32 mask32;
typedef u64 mask64;

typedef float  f32;
typedef double f64;
CompileTimeAssert(sizeof(f32) == 4);
CompileTimeAssert(sizeof(f64) == 8);

constexpr double F64_MAX = 1.7976931348623158e308;
constexpr float  F32_MAX = 3.402823466e38f;

#define Low8(v16)   (u8 )(v16)
#define Low16(v32)  (u16)(v32)
#define Low32(v64)  (u32)(v64)
#define High8(v16)  (u8 )(((u16)(v16)) >> 8)
#define High16(v32) (u16)(((u32)(v32)) >> 16)
#define High32(v64) (u32)(((u64)(v64)) >> 32)


////////////////////////////////////////////////////////////////////////////////
// Optimization macros and functions
////////////////////////////////////////////////////////////////////////////////


#if defined(COMPILER_MSVC)
#define ForceInline __forceinline
#elif defined(COMPILER_CLANG) || defined(COMPILER_GCC)
#define ForceInline inline __attribute__((always_inline))
#else
#define ForceInline inline
#endif

#if defined(COMPILER_CLANG) || defined(COMPILER_GCC)
#define ProbablyTrue(x) (__builtin_expect(!!(x), 1))
#define ProbablyFalse(x) (__builtin_expect((x), 0))
#else
#define ProbablyTrue(x) (x)
#define ProbablyFalse(x) (x)
#endif


#define IsPowerOfTwo(number) (((number) & ((number) - 1)) == 0)

ForceInline void clear_flag(flags8*  flags, flags8  flag) { *flags &= ~flag; }
ForceInline void clear_flag(flags16* flags, flags16 flag) { *flags &= ~flag; }
ForceInline void clear_flag(flags32* flags, flags32 flag) { *flags &= ~flag; }
ForceInline void clear_flag(flags64* flags, flags64 flag) { *flags &= ~flag; }

ForceInline void set_flag(flags8*  flags, flags8  flag) { *flags |= flag; }
ForceInline void set_flag(flags16* flags, flags16 flag) { *flags |= flag; }
ForceInline void set_flag(flags32* flags, flags32 flag) { *flags |= flag; }
ForceInline void set_flag(flags64* flags, flags64 flag) { *flags |= flag; }

ForceInline void assign_flag(flags8*  flags, flags8  flag, bool set) { *flags = set ? (*flags | flag) : (*flags & ~flag); }
ForceInline void assign_flag(flags16* flags, flags16 flag, bool set) { *flags = set ? (*flags | flag) : (*flags & ~flag); }
ForceInline void assign_flag(flags32* flags, flags32 flag, bool set) { *flags = set ? (*flags | flag) : (*flags & ~flag); }
ForceInline void assign_flag(flags64* flags, flags64 flag, bool set) { *flags = set ? (*flags | flag) : (*flags & ~flag); }


// checked casts: unsigned -> signed
ForceInline s8  check_s8 (u8  v) { DebugAssert(v <=  S8_MAX); return (s8)  v; }
ForceInline s8  check_s8 (u16 v) { DebugAssert(v <=  S8_MAX); return (s8)  v; }
ForceInline s8  check_s8 (u32 v) { DebugAssert(v <=  S8_MAX); return (s8)  v; }
ForceInline s8  check_s8 (u64 v) { DebugAssert(v <=  S8_MAX); return (s8)  v; }
ForceInline s16 check_s16(u16 v) { DebugAssert(v <= S16_MAX); return (s16) v; }
ForceInline s16 check_s16(u32 v) { DebugAssert(v <= S16_MAX); return (s16) v; }
ForceInline s16 check_s16(u64 v) { DebugAssert(v <= S16_MAX); return (s16) v; }
ForceInline s32 check_s32(u32 v) { DebugAssert(v <= S32_MAX); return (s32) v; }
ForceInline s32 check_s32(u64 v) { DebugAssert(v <= S32_MAX); return (s32) v; }
ForceInline s64 check_s64(u64 v) { DebugAssert(v <= S64_MAX); return (s64) v; }

// checked casts: signed -> unsigned
ForceInline u8  check_u8 (s8  v) { DebugAssert(v >= 0 && v <=  U8_MAX); return (u8)  v; }
ForceInline u8  check_u8 (s16 v) { DebugAssert(v >= 0 && v <=  U8_MAX); return (u8)  v; }
ForceInline u8  check_u8 (s32 v) { DebugAssert(v >= 0 && v <=  U8_MAX); return (u8)  v; }
ForceInline u8  check_u8 (s64 v) { DebugAssert(v >= 0 && v <=  U8_MAX); return (u8)  v; }
ForceInline u16 check_u16(s16 v) { DebugAssert(v >= 0 && v <= U16_MAX); return (u16) v; }
ForceInline u16 check_u16(s32 v) { DebugAssert(v >= 0 && v <= U16_MAX); return (u16) v; }
ForceInline u16 check_u16(s64 v) { DebugAssert(v >= 0 && v <= U16_MAX); return (u16) v; }
ForceInline u32 check_u32(s32 v) { DebugAssert(v >= 0 && v <= U32_MAX); return (u32) v; }
ForceInline u32 check_u32(s64 v) { DebugAssert(v >= 0 && v <= U32_MAX); return (u32) v; }
ForceInline u64 check_u64(s64 v) { DebugAssert(v >= 0 && v <= U64_MAX); return (u64) v; }

// checked casts: big signed -> small signed
ForceInline s8  check_s8 (s16 v) { DebugAssert(v >=  S8_MIN && v <=  S8_MAX); return (s8)  v; }
ForceInline s8  check_s8 (s32 v) { DebugAssert(v >=  S8_MIN && v <=  S8_MAX); return (s8)  v; }
ForceInline s8  check_s8 (s64 v) { DebugAssert(v >=  S8_MIN && v <=  S8_MAX); return (s8)  v; }
ForceInline s16 check_s16(s32 v) { DebugAssert(v >= S16_MIN && v <= S16_MAX); return (s16) v; }
ForceInline s16 check_s16(s64 v) { DebugAssert(v >= S16_MIN && v <= S16_MAX); return (s16) v; }
ForceInline s32 check_s32(s64 v) { DebugAssert(v >= S32_MIN && v <= S32_MAX); return (s32) v; }

// checked casts: big unsigned -> small unsigned
ForceInline u8  check_u8 (u16 v) { DebugAssert(v <=  U8_MAX); return (u8)  v; }
ForceInline u8  check_u8 (u32 v) { DebugAssert(v <=  U8_MAX); return (u8)  v; }
ForceInline u8  check_u8 (u64 v) { DebugAssert(v <=  U8_MAX); return (u8)  v; }
ForceInline u16 check_u16(u32 v) { DebugAssert(v <= U16_MAX); return (u16) v; }
ForceInline u16 check_u16(u64 v) { DebugAssert(v <= U16_MAX); return (u16) v; }
ForceInline u32 check_u32(u64 v) { DebugAssert(v <= U32_MAX); return (u32) v; }

// checked casts: signed/unsigned 32/64 -> smm/umm
ForceInline umm check_umm(u32 v) { DebugAssert(v <= UMM_MAX); return (umm) v; }
ForceInline umm check_umm(u64 v) { DebugAssert(v <= UMM_MAX); return (umm) v; }
ForceInline smm check_smm(u32 v) { DebugAssert(v <= SMM_MAX); return (smm) v; }
ForceInline smm check_smm(u64 v) { DebugAssert(v <= SMM_MAX); return (smm) v; }
ForceInline umm check_umm(s32 v) { DebugAssert(v >= 0       && v <= UMM_MAX); return (umm) v; }
ForceInline umm check_umm(s64 v) { DebugAssert(v >= 0       && v <= UMM_MAX); return (umm) v; }
ForceInline smm check_smm(s32 v) { DebugAssert(v >= SMM_MIN && v <= SMM_MAX); return (smm) v; }
ForceInline smm check_smm(s64 v) { DebugAssert(v >= SMM_MIN && v <= SMM_MAX); return (smm) v; }


////////////////////////////////////////////////////////////////////////////////////////////////////////
// Allocation
////////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T, bool zero = true>
inline T* allocate_on_application_heap()
{
    T* ptr;
    if (zero)
        ptr = (T*)calloc(1, sizeof(T));
    else
        ptr = (T*)malloc(sizeof(T));

    if (!ptr)
    {
        printf("Failed to allocate memory on application heap. Terminating.\n");
        exit(-1);
        Unreachable;
    }

    return ptr;
}

template<typename T, bool zero = true, umm alignment = 0>
inline T* allocate_array_on_application_heap(umm count)
{
    T* ptr;

    if (zero)
        ptr = (T*)calloc(count, sizeof(T));
    else
        ptr = (T*)malloc(count * sizeof(T));

    if (!ptr)
    {
        printf("Failed to allocate memory on application heap. Terminating.\n");
        exit(-1);
        Unreachable;
    }

    return ptr;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////
// Time
////////////////////////////////////////////////////////////////////////////////////////////////////////

//
// QPC - Fast, monotonic timestamp with undefined epoch and frequency.
//

typedef s64 QPC;

QPC    current_qpc();
double seconds_from_qpc(QPC qpc);
QPC    qpc_from_seconds(double seconds);


ExitApplicationNamespace
